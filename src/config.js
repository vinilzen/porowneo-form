import { getYears } from './common/functions';

export const YEARS = getYears();
export const BRANDS = ['Skoda', 'Opel', 'Volkswagen', 'Audi', 'Ford', 'Kia', 'Toyota'];
export const POPULAR_BRANDS = ['Skoda', 'Volkswagen', 'Opel'];
export const MODELS = ['Corolla', 'Avensis', 'Camry', 'A100', 'Forester', 'Ceed', 'Polo'];
export const COVER_TYPES = ['OC', 'AC', 'NNW', 'Assistance'];
export const DATE_FORMAT = 'DD-MM-YYYY';
export const TYPES_OF_FUEL = ['Benzyna', 'Benzyna + Gaz', 'Diesel'];
export const TYPES_OF_TRANSMISSION = ['Manualna', 'Automatyczna'];
export const MODELS_INFO_EKSPERT = ['1', '2', '3'];
export const VERSION_INFO_EKSPERT = ['4', '5', '6'];
export const VERSION_EUROTAX = ['7', '8', '9'];
