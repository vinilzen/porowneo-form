import { combineReducers } from 'redux';
import moment from 'moment';
import { DATE_FORMAT } from '../config';
import {
  SELECT_BRAND,
  SELECT_MODEL,
  SELECT_YEAR,
  SELECT_COVER_TYPE,
  SET_CAR_TYPE,
  SET_OWNER_TYPE,
  SELECT_START_DATE,
  SET_STEP,
  SELECT_TYPE_OF_FUEL,
  SELECT_TYPE_OF_TRANSMISSION,
  SELECT_MODEL_INFOEKSPERT,
  SELECT_VERSION_INFOEKSPERT,
  SELECT_VERSION_EUROTAX,
} from '../actions';

const selectedCoverType = (state = 'OC', action) => {
  switch (action.type) {
  case SELECT_COVER_TYPE:
    return action.coverType;
  default:
    return state;
  }
};

const setCarType = (state = 1, action) => {
  switch (action.type) {
  case SET_CAR_TYPE:
    return action.carType;
  default:
    return state;
  }
};

const setOwnerType = (state = 1, action) => {
  switch (action.type) {
  case SET_OWNER_TYPE:
    return action.ownerType;
  default:
    return state;
  }
};

const selectedBrand = (state = '', action) => {
  switch (action.type) {
  case SELECT_BRAND:
    return action.brand;
  default:
    return state;
  }
};

const selectedModel = (state = '', action) => {
  switch (action.type) {
  case SELECT_MODEL:
    return action.model;
  default:
    return state;
  }
};

const selectedTypeOfFuel = (state = '', action) => {
  switch (action.type) {
  case SELECT_TYPE_OF_FUEL:
    return action.fuel;
  default:
    return state;
  }
};

const selectedTransmission = (state = '', action) => {
  switch (action.type) {
  case SELECT_TYPE_OF_TRANSMISSION:
    return action.transmission;
  default:
    return state;
  }
};

const selectedYear = (state = 2010, action) => {
  switch (action.type) {
  case SELECT_YEAR:
    return action.year;
  default:
    return state;
  }
};

const selectedStartDate = (state = moment().format(DATE_FORMAT), action) => {
  switch (action.type) {
  case SELECT_START_DATE:
    return action === '' ? moment().format(DATE_FORMAT) : action.startDate;
  default:
    return state;
  }
};

const currentStep = (state = 1, action) => {
  switch (action.type) {
  case SET_STEP:
    return action.step;
  default:
    return state;
  }
};

const selectedModelInfoEkspert = (state = '', action) => {
  switch (action.type) {
  case SELECT_MODEL_INFOEKSPERT:
    return action.model;
  default:
    return state;
  }
};

const selectedVersionInfoEkspert = (state = '', action) => {
  switch (action.type) {
  case SELECT_VERSION_INFOEKSPERT:
    return action.version;
  default:
    return state;
  }
};

const selectedVersionEurotax = (state = '', action) => {
  switch (action.type) {
  case SELECT_VERSION_EUROTAX:
    return action.version;
  default:
    return state;
  }
};


const rootReducer = combineReducers({
  startDate: selectedStartDate,
  coverType: selectedCoverType,
  year: selectedYear,
  brand: selectedBrand,
  model: selectedModel,
  step: currentStep,
  carType: setCarType,
  ownerType: setOwnerType,
  fuel: selectedTypeOfFuel,
  transmission: selectedTransmission,
  versionEurotax: selectedVersionEurotax,
  modelInfoEkspert: selectedModelInfoEkspert,
  versionInfoEkspert: selectedVersionInfoEkspert,
});

export default rootReducer;
