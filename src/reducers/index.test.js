import moment from 'moment';
import reducer from './index';
import { SELECT_BRAND, SELECT_START_DATE, SELECT_COVER_TYPE, SELECT_YEAR } from '../actions';

describe('Form reducer', () => {
  test('Initial state', () => {
    const state = reducer(undefined, {});
    expect(state)
      .toEqual({
        startDate: moment().format('DD-MM-YYYY'),
        coverType: 'OC',
        carType: 1,
        ownerType: 1,
        year: 0,
        step: 1,
        brand: '',
        model: '',
        fuel: '',
        transmission: '',
        modelInfoEkspert: '',
        versionEurotax: '',
        versionInfoEkspert: '',
      });
  });

  test('Set start date', () => {
    const state = reducer({
      coverType: 'OC',
      year: 0,
      brand: '',
    }, {
      type: SELECT_START_DATE,
      startDate: moment().add(7, 'days').format('DD-MM-YYYY'),
    });
    expect(state).toMatchObject({
      startDate: moment().add(7, 'days').format('DD-MM-YYYY'),
    });
  });

  test('Set Cover Type', () => {
    const state = reducer({
      startDate: moment().add(2, 'days').format('DD-MM-YYYY'),
      coverType: 'NNW',
    }, {
      type: SELECT_COVER_TYPE,
      coverType: 'NNW',
    });
    expect(state).toMatchObject({
      coverType: 'NNW',
    });
  });

  test('Set Year 1972', () => {
    const state = reducer({
      startDate: moment().format('DD-MM-YYYY'),
      coverType: 'OC',
      year: '',
      brand: '',
    }, {
      type: SELECT_YEAR,
      year: 1972,
    });

    expect(state).toMatchObject({
      year: 1972,
    });
  });

  test('Set Brand Ford', () => {
    const state = reducer({
      startDate: '03.08.2018',
      coverType: 'OC',
      year: 1972,
      brand: '',
    }, {
      type: SELECT_BRAND,
      brand: 'Ford',
    });
    expect(state).toMatchObject({
      brand: 'Ford',
    });
  });
});
