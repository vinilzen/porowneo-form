import React from 'react';
import renderer from 'react-test-renderer';
import OwnerType from '../OwnerType';

test('Owner types group buttons', () => {
  const component = renderer.create(<OwnerType
    value={1}
    onChange={() => {}}
    list={[1, 0]}
    slug="ownerType"
    title="Pojazd zarejestrowany na osobę fizyczną?"
  />);

  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
