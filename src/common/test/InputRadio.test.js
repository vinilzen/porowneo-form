import React from 'react';
import renderer from 'react-test-renderer';
import PopularBrands from '../PopularBrands';
import { POPULAR_BRANDS } from '../../config';

test('Popular Brands buttons', () => {
  const component = renderer.create(<PopularBrands
    popularBrands={POPULAR_BRANDS}
    onChange={() => {}}
    year={2018}
  />);

  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
