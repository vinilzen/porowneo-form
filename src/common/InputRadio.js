import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputRadio extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(value) {
    this.props.onChange(this.props.slug, value);
  }

  render() {
    const {
      title,
      list,
      value,
      hidden,
    } = this.props;

    return (
      <div className={hidden ? 'hidden' : ''}>
        <small className="text-muted">{title}:</small>
        <div className="btn-group">
          {list.map((item) => {
            let label = item;
            if (item === 1) label = 'Tak';
            if (item === 0) label = 'Nie';

            return (
              <button
                key={item}
                onClick={() => this.handleClick(item)}
                className={`btn btn-secondary ${value === item ? 'active' : ''}`}
              >
                {label}
              </button>
            );
          })}
        </div>
      </div>
    );
  }
}

InputRadio.defaultProps = {
  hidden: false,
  value: '',
  slug: '',
};

InputRadio.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  list: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])).isRequired,
  title: PropTypes.string.isRequired,
  slug: PropTypes.string,
  hidden: PropTypes.bool,
};

export default InputRadio;
