export const getTodayDate = () => {
  const d = new Date();
  const todayDay = d.getDate() < 10 ? `0${d.getDate()}` : d.getDate();
  const todayMonth = d.getMonth() < 9 ? `0${(d.getMonth() + 1)}` : d.getMonth() + 1;
  return `${todayDay}.${todayMonth}.${d.getFullYear()}`;
};

export const getYears = (minYear = 1970) => {
  let currentYear = new Date().getFullYear();
  const years = [];
  while (currentYear >= minYear) {
    years.push(currentYear);
    currentYear -= 1;
  }
  return years;
};
