import React, { Component } from 'react';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';

class DropDown extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
    this.toggleDropDown = this.toggleDropDown.bind(this);
  }

  handleClickOutside() {
    this.setState({
      isOpen: false,
    });
  }

  toggleDropDown() {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));
  }

  handleClick(value) {
    this.props.onChange(this.props.slug, value);
    this.toggleDropDown();
  }

  render() {
    const { isOpen } = this.state;

    const {
      title,
      value,
      disabled,
      list,
      hidden,
    } = this.props;

    return (
      <div className={`form-group ${hidden ? 'hidden' : ''}`}>
        <small className="dropdown-helper form-text text-muted">
          {title}
        </small>
        <div className="dropdown">
          <button
            className="btn btn-secondary btn-block dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            disabled={disabled}
            onClick={this.toggleDropDown}
          >
            {!value ? 'Wybierz' : value}
          </button>
          <div
            className={`dropdown-menu ${isOpen ? 'show' : ''}`}
            aria-labelledby="dropdownMenuButton"
          >
            {list.map((item, key) => (
              <button
                key={item}
                className="dropdown-item"
                data-value={key}
                onClick={() => this.handleClick(item)}
              >
                {item}
              </button>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

DropDown.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  slug: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  list: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])).isRequired,
};

export default onClickOutside(DropDown);
