import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/pl';
import { connect } from 'react-redux';

import { DATE_FORMAT } from '../config';
import {
  selectBrand,
  selectModel,
  selectTypeOfFuel,
  selectTransmission,
  selectYear,
  selectCoverType,
  setCarType,
  setOwnerType,
  selectStartDate,
  setStep, selectModelInfoEkspert, selectVersionInfoEkspert, selectVersionEurotax,
} from '../actions';
import StepsNavigateButton from '../steps/StepsNavigateButton';
import StepOne from '../steps/StepOne';
import StepTwo from '../steps/StepTwo';
import StepTree from '../steps/StepTree';
import StepFour from '../steps/StepFour';

import '../app.css';

moment.locale('pl');

class App extends Component {
  static propTypes = {
    startDate: PropTypes.string.isRequired,
    coverType: PropTypes.string.isRequired,
    year: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
    brand: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    fuel: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    step: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props);
    this.nextStep = this.nextStep.bind(this);
    this.prevStep = this.prevStep.bind(this);
  }

  handleChange = (key, value) => {
    const { dispatch } = this.props;

    switch (key) {
    case 'year':
      return dispatch(selectYear(value));
    case 'brand':
      return dispatch(selectBrand(value));
    case 'model':
      return dispatch(selectModel(value));
    case 'fuel':
      return dispatch(selectTypeOfFuel(value));
    case 'transmission':
      return dispatch(selectTransmission(value));
    case 'coverType':
      return dispatch(selectCoverType(value));
    case 'carType':
      return dispatch(setCarType(value));
    case 'ownerType':
      return dispatch(setOwnerType(value));
    case 'startDate':
      return dispatch(selectStartDate(value.format(DATE_FORMAT)));
    case 'modelInfoEkspert':
      return dispatch(selectModelInfoEkspert(value));
    case 'versionInfoEkspert':
      return dispatch(selectVersionInfoEkspert(value));
    case 'versionEurotax':
      return dispatch(selectVersionEurotax(value));
    default:
      return null;
    }
  }

  nextStep() {
    const {
      step,
      dispatch,
    } = this.props;
    return dispatch(setStep(step + 1));
  }

  prevStep() {
    const {
      step,
      dispatch,
    } = this.props;
    return dispatch(setStep(step - 1));
  }

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <p>Step: {this.props.step}</p>

          <StepOne {...this.props} handleChange={this.handleChange} />

          <StepTwo {...this.props} handleChange={this.handleChange} />

          <StepTree {...this.props} handleChange={this.handleChange} />

          <StepFour {...this.props} handleChange={this.handleChange} />

          <hr />

          <StepsNavigateButton func={this.prevStep} text="Wróć" />
          <StepsNavigateButton func={this.nextStep} text="Dalej" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    step,
    startDate,
    coverType,
    carType,
    ownerType,
    year,
    brand,
    model,
    fuel,
    transmission,
    versionEurotax,
    modelInfoEkspert,
    versionInfoEkspert,
  } = state;

  return {
    step,
    startDate,
    coverType,
    carType,
    ownerType,
    year,
    brand,
    model,
    fuel,
    transmission,
    versionEurotax,
    modelInfoEkspert,
    versionInfoEkspert,
  };
};

export default connect(mapStateToProps)(App);
