export const SELECT_START_DATE = 'SELECT_START_DATE';
export const SELECT_BRAND = 'SELECT_BRAND';
export const SELECT_YEAR = 'SELECT_YEAR';
export const SELECT_COVER_TYPE = 'SELECT_COVER_TYPE';
export const SET_CAR_TYPE = 'SET_CAR_TYPE';
export const SET_OWNER_TYPE = 'SET_OWNER_TYPE';
export const TOGGLE_BRAND_SELECT = 'TOGGLE_BRAND_SELECT';
export const SET_STEP = 'SET_STEP';
export const SELECT_MODEL = 'SELECT_MODEL';
export const SELECT_TYPE_OF_FUEL = 'SELECT_TYPE_OF_FUEL';
export const SELECT_TYPE_OF_TRANSMISSION = 'SELECT_TYPE_OF_TRANSMISSION';
export const SELECT_MODEL_INFOEKSPERT = 'SELECT_MODEL_INFOEKSPERT';
export const SELECT_VERSION_INFOEKSPERT = 'SELECT_VERSION_INFOEKSPERT';
export const SELECT_VERSION_EUROTAX = 'SELECT_VERSION_EUROTAX';

export const selectModelInfoEkspert = model => ({
  type: SELECT_MODEL_INFOEKSPERT,
  model,
});

export const selectVersionInfoEkspert = version => ({
  type: SELECT_VERSION_INFOEKSPERT,
  version,
});

export const selectVersionEurotax = version => ({
  type: SELECT_VERSION_EUROTAX,
  version,
});

export const selectBrand = brand => ({
  type: SELECT_BRAND,
  brand,
});

export const selectModel = model => ({
  type: SELECT_MODEL,
  model,
});

export const selectTypeOfFuel = fuel => ({
  type: SELECT_TYPE_OF_FUEL,
  fuel,
});
export const selectTransmission = transmission => ({
  type: SELECT_TYPE_OF_TRANSMISSION,
  transmission,
});

export const selectYear = year => ({
  type: SELECT_YEAR,
  year,
});

export const selectCoverType = coverType => ({
  type: SELECT_COVER_TYPE,
  coverType,
});

export const setCarType = carType => ({
  type: SET_CAR_TYPE,
  carType,
});

export const setOwnerType = ownerType => ({
  type: SET_OWNER_TYPE,
  ownerType,
});

export const selectStartDate = startDate => ({
  type: SELECT_START_DATE,
  startDate,
});

export const setStep = step => ({
  type: SET_STEP,
  step,
});
