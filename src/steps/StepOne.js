import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import DatePicker from 'react-datepicker/es/index';
import 'react-datepicker/dist/react-datepicker.css';

import {
  YEARS,
  BRANDS,
  POPULAR_BRANDS,
  MODELS,
  COVER_TYPES,
  DATE_FORMAT,
  TYPES_OF_FUEL,
  TYPES_OF_TRANSMISSION,
  MODELS_INFO_EKSPERT,
  VERSION_INFO_EKSPERT,
  VERSION_EUROTAX,
} from '../config';
import StepContainer from './StepContainer';
import DropDown from '../common/DropDown';
import InputRadio from '../common/InputRadio';

const StepOne = ({
  step,
  year,
  brand,
  model,
  fuel,
  coverType,
  startDate,
  handleChange,
  transmission,
  modelInfoEkspert,
  versionInfoEkspert,
  versionEurotax,
}) => (
  <StepContainer active={step === 1}>

    <small className="form-text text-muted">
      Od kiedy potrzebujesz ubezpieczenia?
    </small>

    <DatePicker
      dateFormat={DATE_FORMAT}
      selected={moment(startDate, DATE_FORMAT)}
      className="form-control"
      onChange={value => handleChange('startDate', value)}
      dropdownMode="select"
    />

    <InputRadio
      slug="coverType"
      list={COVER_TYPES}
      value={coverType}
      onChange={handleChange}
      title="Wybierz zakres"
    />

    <DropDown
      slug="year"
      value={year}
      list={YEARS}
      title="Rok produkcji pojazdu"
      disabled={false}
      onChange={handleChange}
    />

    <InputRadio
      slug="brand"
      list={POPULAR_BRANDS}
      onChange={handleChange}
      year={parseInt(year, 10)}
      title="Najpopularniejsze"
    />

    <DropDown
      slug="brand"
      value={brand}
      list={BRANDS}
      title="Marka pojazdu"
      disabled={!year}
      onChange={handleChange}
    />

    <DropDown
      slug="model"
      value={model}
      list={MODELS}
      title="Model pojazdu"
      disabled={!brand}
      onChange={handleChange}
    />

    <InputRadio
      list={TYPES_OF_FUEL}
      onChange={handleChange}
      value={fuel}
      title="Rodzaj paliwa"
      slug="fuel"
      hidden={!model}
    />

    <InputRadio
      list={TYPES_OF_TRANSMISSION}
      onChange={handleChange}
      value={transmission}
      title="Skrzynia biegów"
      slug="transmission"
      hidden={!fuel}
    />

    <DropDown
      slug="versionEurotax"
      value={versionEurotax}
      list={VERSION_EUROTAX}
      title="Wersja pojazdu (wg bazy Eurotax)"
      disabled={!model}
      onChange={handleChange}
      hidden={!transmission}
    />

    <DropDown
      slug="modelInfoEkspert"
      value={modelInfoEkspert}
      list={MODELS_INFO_EKSPERT}
      title="Model pojazdu (wg bazy Info-Ekspert)"
      disabled={!model}
      onChange={handleChange}
      hidden={!transmission}
    />

    <DropDown
      slug="versionInfoEkspert"
      value={versionInfoEkspert}
      list={VERSION_INFO_EKSPERT}
      title="Wersja pojazdu (wg bazy Info-Ekspert)"
      disabled={!model}
      onChange={handleChange}
      hidden={!transmission}
    />

  </StepContainer>
);

StepOne.propTypes = {
  step: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  brand: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  fuel: PropTypes.string.isRequired,
  coverType: PropTypes.string.isRequired,
  startDate: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  transmission: PropTypes.string.isRequired,
  versionEurotax: PropTypes.string.isRequired,
  modelInfoEkspert: PropTypes.string.isRequired,
  versionInfoEkspert: PropTypes.string.isRequired,
};

export default StepOne;
