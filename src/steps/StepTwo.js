import React from 'react';
import PropTypes from 'prop-types';
import StepContainer from './StepContainer';
import InputRadio from '../common/InputRadio';

const StepTwo = ({ step, carType, ownerType, handleChange }) => (
  <StepContainer active={step === 2}>
    <h1>Step: {step}</h1>
    <InputRadio
      onChange={handleChange}
      value={carType}
      list={[1, 0]}
      slug="carType"
      title="Pojazd zarejestrowany jako osobowy?"
    />
    <InputRadio
      onChange={handleChange}
      value={ownerType}
      list={[1, 0]}
      slug="ownerType"
      title="Pojazd zarejestrowany na osobę fizyczną?"
    />
  </StepContainer>
);

StepTwo.propTypes = {
  step: PropTypes.number.isRequired,
  carType: PropTypes.number.isRequired,
  ownerType: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default StepTwo;
