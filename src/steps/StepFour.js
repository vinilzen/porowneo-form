import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import DatePicker from 'react-datepicker/es/index';
import StepContainer from './StepContainer';
import { DATE_FORMAT } from '../config';

const StepFour = ({ step, startDate }) => (
  <StepContainer active={step === 4}>
    <h1>Step: {step}</h1>
    <DatePicker
      dateFormat={DATE_FORMAT}
      selected={moment(startDate, DATE_FORMAT)}
      className="form-control"
      onChange={value => this.handleChange('startDate', value)}
      dropdownMode="select"
    />
  </StepContainer>
);

StepFour.propTypes = {
  step: PropTypes.number.isRequired,
  startDate: PropTypes.string.isRequired,
};

export default StepFour;
