import React from 'react';
import PropTypes from 'prop-types';

const StepContainer = ({ active, children }) => (
  <div className={`carousel-item${active ? ' active' : ''}`}>
    <div className="container">
      {children}
    </div>
  </div>
);

StepContainer.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default StepContainer;
