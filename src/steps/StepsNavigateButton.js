import React from 'react';
import PropTypes from 'prop-types';

const StepsNavigateButton = ({ func, text }) => <button onClick={func}>{text}</button>;

StepsNavigateButton.propTypes = {
  func: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default StepsNavigateButton;
