# POROWNEO-FORM


## Folder Structure

After cloning repository, your project should look like this:

```
my-app/
  README.md
  .eslintrc
  .gitignore
  yarn.lock
  node_modules/
  package.json
  public/
    index.html
  src/
    actions/
    common/
    components/
    containers/
        App.js
    reducers/
    scss/
      components/
    app.css
    index.js
```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.


You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.

Only files inside `public` can be used from `public/index.html`.<br>
Read instructions below for using assets from JavaScript and HTML.

You can, however, create more top-level directories.<br>
They will not be included in the production build so you can use them for things like documentation.


## Installing a Dependency

The generated project includes React and ReactDOM as dependencies. It also includes a set of scripts used by our App as a development dependency. You may install this dependencies with `npm`:

```sh
npm install 
```

Alternatively you may use `yarn`:

```sh
yarn install
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.


## Running tests


```sh
npm test
```


### Coverage Reporting

Jest has an integrated coverage reporter that works well with ES6 and requires no configuration.<br>
Run `npm test -- --coverage` (note extra `--` in the middle) to include a coverage report like this:

![coverage report](http://i.imgur.com/5bFhnTS.png)

Note that tests run much slower with coverage so it is recommended to run it separately from your normal workflow.


## Server Render

For server prerender we use `react-snap`

```sh
npm run postbuild
```

## Deployment

....
